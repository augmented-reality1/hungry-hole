﻿using UnityEngine;
using Sqo;
using System.IO;
using System;
public class CollisionDetector : MonoBehaviour
{
    public static CollisionDetector Instance;

    public CollisionDetector() {
        Instance = this;
    }

    private void Start()
    {
        // var debugText = GameObject.Find("DebugText").GetComponent<UnityEngine.UI.Text>();
        // debugText.text += "\nPrevious scores:";

        // IObjectList<Score> scoreList = Database.LoadObjects<Score>();

        // if (scoreList.Count < 1)
        //     debugText.text += "\nNo scores yet.";

        // foreach (var score in scoreList)
        //     debugText.text += $"\n{score.value}";
    }

    void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.name.StartsWith("ModeSelector"))
        {
            ModeManagerScript.Instance.EnableMode(other.gameObject.name);
        }
        else if(other.gameObject.name.EndsWith("Score"))
        {
            ScoreBoardScript.Instance.resetSpawn(other.gameObject);
        }
        else if(other.gameObject.name.StartsWith("Level"))
        {
            Destroy(other.gameObject);
        }
        else
        {
            if (Mode3Script.Instance.gameObject.activeInHierarchy)
			{
                CubeSpawnerScriptMode3.Instance.onObjectFall(other.gameObject);
            }
			else if (Mode1Script.Instance.gameObject.activeInHierarchy)
			{
                CubeSpawnerScript.Instance.onObjectFall(other.gameObject);
            } else if (Mode2Script.Instance.gameObject.activeInHierarchy) {
                if (other.gameObject.tag != "Bomb") {
                    Mode2Script.Instance.cubeIsEaten();
                } else {
                    Mode2Script.Instance.gameOver();
                }
            }
            //Debug.Log("Score is: " + ++score.value);
        }
    }
}