﻿using System;
using UnityEngine;
using UnityEngine.SceneManagement;

public class CubeSpawnerScript : MonoBehaviour
{
    public static int startQuantity = 10;
    // Singleton
    public static CubeSpawnerScript Instance;

    public CubeSpawnerScript()
    {
        Instance = this;
    }

    public void initSpawn()
    {
        for (int i = 0; i < startQuantity; i++)
        {
            ObjectPooler.Instance.spawnRandomObject();
        }
    }

    public void onObjectFall(GameObject obj)
    {
        ObjectPooler.Instance.onObjectFall(obj);
    }
}
