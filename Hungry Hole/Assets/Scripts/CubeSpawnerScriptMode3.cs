﻿using System;
using UnityEngine;
using UnityEngine.SceneManagement;

public class CubeSpawnerScriptMode3 : MonoBehaviour
{
    public static int startQuantity = 10;
    // Singleton
    public static CubeSpawnerScriptMode3 Instance;

    public CubeSpawnerScriptMode3()
    {
        Instance = this;
    }

    public void initSpawn()
    {
        for (int i = 0; i < startQuantity; i++)
        {
            ObjectPoolerMode3.Instance.spawnRandomObject();
        }
    }

    public void onObjectFall(GameObject obj)
    {
        if (obj.tag == "Bomb") {
            Mode3Script.Instance.gameOver();  
        } else {
            Mode3Script.Instance.resetTimer();
            Mode3Script.Instance.score.value++;
            ObjectPoolerMode3.Instance.onObjectFall(obj);
        }
    }
}
