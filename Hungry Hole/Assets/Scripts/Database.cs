using UnityEngine;
using Sqo;
using System;
using System.Collections.Generic;
using System.Linq;

public static class Database
{
    private static Siaqodb siaqodb = StartDB();

    private static Siaqodb StartDB()
    {
        bool isAndroidOrIOS = Application.platform == RuntimePlatform.Android || Application.platform == RuntimePlatform.IPhonePlayer;
        string path = isAndroidOrIOS ? Application.persistentDataPath : ".";
        Siaqodb db = null;
        try
        {
            db = new Siaqodb(path + "/Siaqodb");
        }
        catch (Exception e)
        {
            Debug.Log(e.ToString());
            HoleScript.Instance.DebugMessage(e.Message);
        }

        return db;
    }

    // Generic methods which do database operations on objects of type T
    public static IObjectList<T> LoadObjects<T>()
    {
        return siaqodb != null ? siaqodb.LoadAll<T>() : null;
    }

    // SiaqoDB stores exclusively only objects;
    public static void StoreObject<T>(T obj)
    {
        siaqodb?.StoreObject(obj);
    }

    public static void DeleteObject<T>(T obj)
    {
        siaqodb?.Delete(obj);
    }

    public static void DeleteType<T>()
    {
        siaqodb?.DropType<T>();
    }

    public static void DeleteAll()
    {
        siaqodb?.DropAllTypes();
    }

    public static List<Score> getTop3Scores()
    {
        var scores = siaqodb.LoadAll<Score>().ToList();
        var topScores = scores.Distinct(new ScoreComparer()).OrderByDescending(x => x.value).Take(scores.Count >= 3 ? 3 : scores.Count).Where(x => x.value > 0);
        return topScores.ToList();
    }
}
