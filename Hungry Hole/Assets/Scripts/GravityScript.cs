﻿using UnityEngine;
using Vuforia;

[RequireComponent(typeof(Rigidbody))]
public class GravityScript : MonoBehaviour
{
	private bool IsTest; //when debugging or testing without an AR device
	
	// [SerializeField]
	// private Vuforia.ImageTargetBehaviour target;
	
	private Vuforia.AnchorBehaviour anchor;
	
	private Rigidbody Rigidbody;

    void Start()
    {
		this.IsTest = HoleScript.Instance.IsTest;

		if(!IsTest)
		{
			VuforiaARController.Instance.RegisterVuforiaStartedCallback(OnVuforiaStarted);
			//this.target = this.transform.parent.GetComponent<Vuforia.ImageTargetBehaviour>();
			//this.anchor = this.transform.parent.GetComponent<Vuforia.AnchorBehaviour>();
			this.anchor = HoleScript.Instance.Anchor;
		}
        this.Rigidbody = this.GetComponent<Rigidbody>();
		this.Rigidbody.useGravity = true;
    }

    void OnVuforiaStarted() => Debug.Log("Vuforia is started");

    void FixedUpdate() {
		// 'Pause' the game (physics) while the target is not visible
		// Checking for 'target is null' since vuforia crashes when object is inactive because it can not find it
		// if (target != null) this.Rigidbody.isKinematic = target.gameObject.activeSelf ? target.CurrentStatus == TrackableBehaviour.Status.NO_POSE : false;
		if(!IsTest)
		{
			if (anchor != null)
			{
				this.Rigidbody.isKinematic = anchor.isActiveAndEnabled ? anchor.CurrentStatus == TrackableBehaviour.Status.NO_POSE : false;
			}
			else this.Rigidbody.isKinematic = true;
		}
	}


}