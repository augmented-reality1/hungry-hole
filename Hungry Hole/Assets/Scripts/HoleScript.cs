﻿using UnityEngine;

#pragma warning disable 0649 // Suppress warnings about unassigned SeriazeFields
public class HoleScript : MonoBehaviour
{	
	public static HoleScript Instance;
	
	public static float Size;
	
	[SerializeField]
	private Transform Camera;
	
	[SerializeField]
	private Transform Target;

	[SerializeField]
	public Vuforia.AnchorBehaviour Anchor;
	
	public bool IsTest;

	[SerializeField]
	private float maxDistance = 10;
	
	[SerializeField]
	private float playerHeight = 1.4f;
	
	private Vector3 position;
	
	private Plane plane;
	
	public HoleScript()
	{
		Instance = this;
	}
	
    void Start()
    {
		plane = new Plane(Target.up, Target.position + new Vector3(0, -playerHeight, 0));
    }
	
    void Update()
    {
		Debug.DrawRay(Vector3.zero, plane.normal, Color.blue);
		
		Ray ray = new Ray(Camera.position, Camera.forward);
		
		Debug.DrawRay(ray.origin, ray.direction, Color.red);
		
		float enter = 0.0f;
		if(plane.Raycast(ray, out enter))
		{
			Vector3 hit = ray.GetPoint(enter);
			position = new Vector3(Mathf.Clamp(hit.x, -maxDistance, maxDistance), hit.y, Mathf.Clamp(hit.z, -maxDistance, maxDistance)); // don't let the hole go infinitely far
		}
		
		if(Target.gameObject.activeSelf)
		{
			//set the position of the transform directly, the physics is handled by child objects
			//position should be set in Update() for smoother gameplay
			this.transform.SetPositionAndRotation(position, this.transform.rotation);
		}
    }
	
	public void DebugMessage(string s)
	{
		GameObject.Find("DebugText").GetComponent<UnityEngine.UI.Text>().text += s;
	}
}

#pragma warning restore 0649