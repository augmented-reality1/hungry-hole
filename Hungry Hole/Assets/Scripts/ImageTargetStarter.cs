﻿using UnityEngine;
using Vuforia;

public class ImageTargetStarter : MonoBehaviour, ITrackableEventHandler
{
    private bool found = false;
    private TrackableBehaviour mTrackableBehaviour;
    private CubeSpawnerScript cubeSpawnerScript;
    void Start()
    {
        cubeSpawnerScript = CubeSpawnerScript.Instance;
        mTrackableBehaviour = GetComponent<TrackableBehaviour>();
        if (mTrackableBehaviour)
            mTrackableBehaviour.RegisterTrackableEventHandler(this);
    }

    // public void onTrackingFound() {
    //     cubeSpawnerScript.initSpawn();
    // }

    public void OnTrackableStateChanged(TrackableBehaviour.Status previousStatus, TrackableBehaviour.Status newStatus)
    {
        bool tracked = newStatus == TrackableBehaviour.Status.TRACKED;
        bool detected = newStatus == TrackableBehaviour.Status.DETECTED;
        bool extTracked = newStatus == TrackableBehaviour.Status.EXTENDED_TRACKED;
        bool any = tracked || detected || extTracked;

        if (any && !found)
        {
            found = true;
            //cubeSpawnerScript.initSpawn();
            // mTrackableBehaviour.UnregisterOnTrackableStatusChanged(this);
        }
    }
}

