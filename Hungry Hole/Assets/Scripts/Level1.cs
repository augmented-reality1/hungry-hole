﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Level1 : MonoBehaviour
{
    public static Level1 Instance;
    
    private int objectsInLevel;
    private int eatenCubes = 0;
    
    public Level1()
    {
        Instance = this;
    }

    void OnEnable()
	{
        this.objectsInLevel = Mode2Script.Instance.countCubes();
	}

    public void cubeIsEaten() {
        this.eatenCubes += 1;
        if (this.eatenCubes >= this.objectsInLevel) {
            Mode2Script.Instance.Victory();
        }
    }
}
