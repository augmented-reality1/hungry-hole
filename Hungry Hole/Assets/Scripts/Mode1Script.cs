﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Mode1Script : MonoBehaviour
{
	public static Mode1Script Instance;
	
	public Mode1Script()
	{
		Instance = this;
	}
	
	void OnEnable()
	{
        ObjectPooler.Instance.Init();
		CubeSpawnerScript.Instance.initSpawn();
    }
	
	public void OnButtonClick()
	{
		if(!this.gameObject.activeInHierarchy)
		{
			return;
		}
		
		HoleScript.Instance.transform.localScale = new Vector3(0.5f, 0.5f, 0.5f);
		
		var cubes = this.GetComponentsInChildren<GravityScript>();
		foreach(var cube in cubes)
		{
			Destroy(cube.gameObject);
		}
		
		this.gameObject.SetActive(false);
		
		// ModeManagerScript.Instance.ResetModeSelectors();
		// ModeManagerScript.Instance.EnableModeSelectors();
	}
}