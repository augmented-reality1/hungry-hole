﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Mode2Script : MonoBehaviour
{
    public static Mode2Script Instance;
    public GameObject level;
    public Text text;
    private GameObject ActiveLevel;
    private string levelResult = "";
    public Mode2Script()
    {
        Instance = this;
    }

    void OnEnable()
	{
        ActiveLevel = Instantiate(level);
        ActiveLevel.transform.parent = this.gameObject.transform;
		ActiveLevel.transform.localPosition = Vector3.zero;
		text.color = Color.white;
        ActiveLevel.SetActive(true);
	}

    public int countCubes() {
        var gameObjects = ActiveLevel.GetComponentsInChildren<Transform>();
        int objects = 0;
        foreach (Transform t in gameObjects)
        {
            if (t.gameObject.tag == "Cube") {
                objects += 1;
            }
        }

        return objects;
    }

    public void gameOver() {
        Destroy(ActiveLevel);
		text.color = Color.red;
        levelResult = "You failed!";
    }    

    public void OnButtonClick()
	{
		if(!this.gameObject.activeInHierarchy)
		{
			return;
		}
		
		HoleScript.Instance.transform.localScale = new Vector3(0.5f, 0.5f, 0.5f);
		
		levelResult = "";
		
        Destroy(this.ActiveLevel);

		this.gameObject.SetActive(false);

        //text.text = "";
		
		// ModeManagerScript.Instance.ResetModeSelectors();
		// ModeManagerScript.Instance.EnableModeSelectors();
	}

    public void cubeIsEaten() {
        if (Level1.Instance.gameObject.activeInHierarchy) {
            Level1.Instance.cubeIsEaten();
        }
    }

    public void Victory() {
        Destroy(this.ActiveLevel);
        Debug.Log("Level cleared!");
        levelResult = "Level cleared!";
    }

    void FixedUpdate()
    {
        if (levelResult != "") {
            text.text = levelResult;
        }
    }
}
