﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Mode3Script : MonoBehaviour
{
    float currentTime = 0f;
    float startTime = 5f;
    bool finished = false;
    public Text CountDownText;
    public Score score;
    public static Mode3Script Instance;
	
	private bool firstTimer;

    public Mode3Script()
    {
        Instance = this;
    }

    void OnEnable()
    {
        score = new Score() { value = 0 };
        finished = false;
		firstTimer = true;
		CountDownText.text = "Eat a cube to start!";
        currentTime = startTime;
        ObjectPoolerMode3.Instance.Init();
        CubeSpawnerScriptMode3.Instance.initSpawn();
    }

    public void OnButtonClick()
    {
        if (!this.gameObject.activeInHierarchy)
            return;

        //this.CountDownText.text = "";
        HoleScript.Instance.transform.localScale = new Vector3(0.5f, 0.5f, 0.5f);
        this.gameObject.SetActive(false);

        var cubes = this.GetComponentsInChildren<GravityScript>();
        foreach (var cube in cubes)
            Destroy(cube.gameObject);
    }

    public void resetTimer()
    {
		firstTimer = false;
        currentTime = startTime;
    }

    // Update is called once per frame
    void Update()
    {
        if (!this.finished)
        {
            if (currentTime > 0 && !firstTimer)
            {
                currentTime -= 1 * Time.deltaTime;
                print(currentTime);
                CountDownText.text = currentTime.ToString("N3");
            }

            if (currentTime >= 2.5f) { CountDownText.color = Color.white; }
            if (currentTime < 2.5f) { CountDownText.color = Color.red; }

            if (currentTime <= 0f)
            {
                this.finished = true;
                var cubes = this.GetComponentsInChildren<GravityScript>();
                foreach (var cube in cubes)
                    Destroy(cube.gameObject);

                printScore();
                saveScore();
				this.gameObject.SetActive(false);
                // ModeManagerScript.Instance.ResetModeSelectors();
                // ModeManagerScript.Instance.EnableModeSelectors();
            }
        }
    }

    public void gameOver() {
        currentTime = -1f;
    }

    void saveScore()
    {
        if (score.value > 0)
        {
            Database.StoreObject<Score>(score);
        }
    }

    void printScore()
    {
        // if (!this.finished) {
        CountDownText.text = "Game over. Your score: " + score.value;
        // }
        // this.finished = true;
    }
}
