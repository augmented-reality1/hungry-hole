﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Vuforia;

public class ModeManagerScript : MonoBehaviour
{
	public static ModeManagerScript Instance;
	public ModeManagerScript()
	{
		Instance = this;
	}
	
	public GameObject ModeSelector1;
	public Transform Mode1Position;
	
	public GameObject ModeSelector2;
	public Transform Mode2Position;
	
	public GameObject ModeSelector3;
	public Transform Mode3Position;
	
	public GameObject ModeSelectorScore;
	public Transform ScorePosition;
	
	public GameObject Mode1;
	
	public GameObject Mode2;
	
	public GameObject Mode3;
	
	public GameObject ScoreBoard;
	
	public UnityEngine.UI.Text Text;
	
	public void EnableMode(string mode)
	{
		if(mode.EndsWith("1"))
		{
			Mode1?.SetActive(true);
		}
		else if(mode.EndsWith("2"))
		{
			Mode2?.SetActive(true);
		}
		else if(mode.EndsWith("3"))
		{
			Mode3?.SetActive(true);
		}
		else if(mode.EndsWith("Score"))
		{
			ScoreBoard?.SetActive(true);
		}
		else
		{
			//?
		}
		
		DisableModeSelectors();
	}
	
	public void ResetModeSelectors()
	{
		ModeSelector1?.transform.SetPositionAndRotation(Mode1Position.position, Mode1Position.rotation);
		ModeSelector2?.transform.SetPositionAndRotation(Mode2Position.position, Mode2Position.rotation);
		ModeSelector3?.transform.SetPositionAndRotation(Mode3Position.position, Mode3Position.rotation);
		ModeSelectorScore?.transform.SetPositionAndRotation(ScorePosition.position, ScorePosition.rotation);
	}
	
	public void EnableModeSelectors()
	{
		ModeSelector1?.SetActive(true);
		ModeSelector2?.SetActive(true);
		ModeSelector3?.SetActive(true);
		ModeSelectorScore?.SetActive(true);
	}
	
	public void DisableModeSelectors()
	{
		ModeSelector1?.SetActive(false);
		ModeSelector2?.SetActive(false);
		ModeSelector3?.SetActive(false);
		ModeSelectorScore?.SetActive(false);
	}
	
	public void OnButtonClick()
	{
		Mode1Script.Instance?.OnButtonClick();
		Mode2Script.Instance?.OnButtonClick();
		Mode3Script.Instance?.OnButtonClick();
		ScoreBoardScript.Instance?.OnButtonClick();
		
		Text.color = Color.white;
		Text.text = "";
		
		ResetModeSelectors();
		EnableModeSelectors();
	}
}
