﻿using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

[System.Serializable]
public class ObjectPooler : MonoBehaviour
{
    public int spawnRadius = 10;
    public GameObject parent;
    [System.Serializable]
    public class Pool
    {
        public string tag;
        public GameObject prefab;
        public int size;
    }

    // Singleton
    public static ObjectPooler Instance;

    public ObjectPooler()
    {
        Instance = this;
    }

    public List<Pool> pools;
    public UnityEngine.Object[] materials;
    public Dictionary<string, Queue<GameObject>> poolDictionary;
	
    public void Init()
    {
        // if (SceneManager.GetActiveScene().name == "ARScene") {
        //     parent = GameObject.Find("ImageTarget");
        // }
        materials = Resources.LoadAll("CubeMaterials", typeof(Material));
        poolDictionary = new Dictionary<string, Queue<GameObject>>();
        foreach (Pool pool in pools)
        {
            Queue<GameObject> objectPool = new Queue<GameObject>();
			
			pool.prefab.SetActive(false);
			
            for (int i = 0; i < pool.size; i++)
            {
                GameObject obj = Instantiate(pool.prefab);
                obj.transform.SetParent(parent.transform);
                obj.SetActive(false);
                objectPool.Enqueue(obj);
            }

            poolDictionary.Add(pool.tag, objectPool);
        }
    }

    public static Vector3 RandomPointOnUnitCircle(float radius)
    {
        Vector3 pos = UnityEngine.Random.insideUnitCircle * radius;
        int up = 15;
        return new Vector3(pos.x, up, pos.y);
    }

    public void onObjectFall(GameObject obj)
    {
		HoleScript.Instance.transform.localScale += new Vector3(0.02f, 0, 0.02f);
		
        string tag = obj.tag;
        obj.SetActive(false);

        Vector3 pos = RandomPointOnUnitCircle(spawnRadius);
        pos += transform.position;

        obj.transform.position = pos;
        obj.transform.rotation = Quaternion.identity;
		obj.transform.localScale += Vector3.one * 0.1f;
        obj.GetComponent<Renderer>().material = (Material)materials[UnityEngine.Random.Range(0, materials.Length)];
		obj.GetComponent<Rigidbody>().velocity = Vector3.zero;

        // also possible to give random scale
        obj.SetActive(true);
    }

    public void spawnRandomObject()
    {
        Vector3 pos = RandomPointOnUnitCircle(spawnRadius);
        pos += transform.position;
        // tag can be something else but it's not implemented yet
        // also implement random scaling here and pass it as a parametr to object pooler method
        SpawnFromPool("Cube", pos, Quaternion.identity);
    }

    // unnecessary (maybe for future)
    public void spawnObject(string tag)
    {
        if (tag == null) spawnRandomObject();
        else
        {
            Vector3 pos = RandomPointOnUnitCircle(1);
            pos += transform.position;
            SpawnFromPool(tag, pos, Quaternion.identity);
        }
    }

    // unnecessary (maybe for future)
    public GameObject SpawnFromPool(string tag, Vector3 position, Quaternion rotation)
    {

        if (!poolDictionary.ContainsKey(tag))
        {
            Debug.LogWarning("Pool with tag " + tag + " doesn't exist.");
            return null;
        }

        GameObject objectToSpawn = poolDictionary[tag].Dequeue();
        objectToSpawn.SetActive(true);
        objectToSpawn.transform.position = position;
        objectToSpawn.transform.rotation = rotation;

        objectToSpawn.GetComponent<Renderer>().material = (Material)materials[UnityEngine.Random.Range(0, materials.Length)];

        poolDictionary[tag].Enqueue(objectToSpawn);

        return objectToSpawn;
    }
}
