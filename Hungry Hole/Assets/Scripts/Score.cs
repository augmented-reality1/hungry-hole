using System.Collections.Generic;
public class Score
{
    public int value { get; set; } = 0;
}

public class ScoreComparer : IEqualityComparer<Score>
{
	public bool Equals(Score x, Score y)
	{
		return x.value == y.value;
	}
	
	public int GetHashCode(Score obj)
	{
		return obj.value;
	}
}