﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ScoreBoardScript : MonoBehaviour
{
    public TextMesh firstText;
    public GameObject firstScore;
    public Transform firstPosition;
    public TextMesh secondText;
    public GameObject secondScore;
    public Transform secondPosition;
    public TextMesh thirdText;
    public GameObject thirdScore;
    public Transform thirdPosition;

    public static ScoreBoardScript Instance;

    public ScoreBoardScript()
    {
        Instance = this;
    }

    void OnEnable()
	{
        HoleScript.Instance.transform.localScale = new Vector3(1f, 0.5f, 1f);

        firstScore.SetActive(false);
        secondScore.SetActive(false);
        thirdScore.SetActive(false);

        var scores = Database.getTop3Scores();
        if(scores.Count >= 1)
        {
            firstText.text = scores[0].value.ToString();
            firstScore.SetActive(true);

            if(scores.Count >= 2)
            {
                secondText.text = scores[1].value.ToString();
                secondScore.SetActive(true);

                if(scores.Count == 3)
                {
                    thirdText.text = scores[2].value.ToString();
                    thirdScore.SetActive(true);
                }
            }
        }
    
    }

    //Whatever
    public void resetSpawn(GameObject obj)
    {
        obj.GetComponent<Rigidbody>().velocity = Vector3.zero;

        if(obj.name.StartsWith("First"))
        {
            firstScore.transform.SetPositionAndRotation(firstPosition.position, firstPosition.rotation);
        }
        if(obj.name.StartsWith("Second"))
        {
            secondScore.transform.SetPositionAndRotation(secondPosition.position, secondPosition.rotation);
        }
        if(obj.name.StartsWith("Third"))
        {
            thirdScore.transform.SetPositionAndRotation(thirdPosition.position, thirdPosition.rotation);
        }
    }

    public void OnButtonClick()
	{
		if(!this.gameObject.activeInHierarchy)
		{
			return;
		}
		
		HoleScript.Instance.transform.localScale = new Vector3(0.5f, 0.5f, 0.5f);

		this.gameObject.SetActive(false);

        firstScore.transform.SetPositionAndRotation(firstPosition.position, firstPosition.rotation);
        secondScore.transform.SetPositionAndRotation(secondPosition.position, secondPosition.rotation);
        thirdScore.transform.SetPositionAndRotation(thirdPosition.position, thirdPosition.rotation);
    }
}
