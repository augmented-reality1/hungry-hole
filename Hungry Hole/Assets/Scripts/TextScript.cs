﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

#pragma warning disable 0649
public class TextScript : MonoBehaviour
{
	[SerializeField]
	private Transform TextPivot;
	[SerializeField]
	private Transform Camera;
    void FixedUpdate()
	{
		TextPivot.LookAt(Camera.transform);
	}
}
#pragma warning restore 0649
