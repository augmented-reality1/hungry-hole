﻿// Upgrade NOTE: replaced 'mul(UNITY_MATRIX_MVP,*)' with 'UnityObjectToClipPos(*)'

Shader "Unlit/FadeShader"
{
    Properties
    {
		_Color ("Color 1", Color) = (1,1,1,0)
		_Color2 ("Color 2", Color) = (0,0,0,1)
		_Scale ("Scale", Float) = 1
    }
    SubShader
    {
        Tags { "RenderType"="transparent" "Queue"="Transparent+2" }
        LOD 100
		ZWrite Off
		Blend SrcAlpha OneMinusSrcAlpha
        Pass
        {
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
			
            #include "UnityCG.cginc"
			
            fixed4 _Color;
			fixed4 _Color2;
			fixed  _Scale;
			
			struct v2f {
				float4 pos : SV_POSITION;
				fixed4 col : COLOR;
			};
			
			v2f vert (appdata_full v)
			{
				v2f o;
				o.pos = UnityObjectToClipPos (v.vertex);
				o.col = lerp(_Color,_Color2, - v.vertex.z * _Scale);
				return o;
			}
			
			float4 frag (v2f i) : COLOR {
				float4 c = i.col;
				return c;
			}
			
            ENDCG
        }
    }
}
